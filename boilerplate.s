.data

# DO NOT MODIFY
args:	.word	0 0 0 0 0 0
# DO NOT MODIFY

# put your variables below:


.text

user_start:
	# put your code below:
	





end:
	li $v0, 10
	syscall






























# DO NOT MODIFY ANYTHING BELOW

main:
	b all_args

all_args:
	beqz $a0, all_args_done
	
	move $s6, $a0
	move $s7, $a1
	
	li $s5, 0

all_args_loop:
	move $t8, $s7
	
	addi $s6, $s6, -1
	beqz $s6, all_args_done
	
	mul $s4, $s5, 4
	add $t8, $s4, $s7
	
	lw $a0, 4($t8)
	jal atoi
	
	move $t8, $s4
	la $t9, args
	add $t9, $t9, $t8
	
	sw $v0, ($t9)
	
	addi $s5, $s5, 1
	b all_args_loop

all_args_done:
	la $t9, args
	
	lw $s0, ($t9)
	lw $s1, 4($t9)
	lw $s2, 8($t9)
	lw $s3, 12($t9)
	lw $s4, 16($t9)
	lw $s5, 20($t9)
	
	b user_start

atoi:
	# $a0: address of zero-terminated char array (one byte per char)
	# 
	# $v0: word converted from string OR original address if nan
	# 
	# $t0: temp char holder
	# $t1: address counter
	# $t2: length of array (after pre-loop)
	# $t3: used to temp store the temp char
	# $t4: count which digit we are on (digit 0 is first)
	# $t5: temp return address
	# $t6: copy of $a0
	# $t7: continously calculated final result
	
	li $v0, 0
	li $t0, 0
	li $t1, 0
	li $t2, 0
	li $t3, 0
	li $t4, 0
	li $t5, 0
	li $t6, 0
	li $t7, 0
	move $t1, $a0
	move $t5, $ra
	move $t6, $a0

atoi_pre_loop:
	lb $t0, ($t1)
	beqz $t0, atoi_loop
	addi $t1, $t1, 1
	addi $t2, $t2, 1
	b atoi_pre_loop

atoi_loop:
	beqz $t2, atoi_is_num
	addi $t1, $t1, -1
	addi $t2, $t2, -1
	lb $t0, ($t1)
	
	addi $t0, $t0, -48
	bltz $t0, atoi_nan
	
	move $t3, $t0
	
	addi $t3, $t3, -9
	bgtz $t3, atoi_nan
	
	# we know $t0 is an actual number
	li $a0, 10
	move $a1, $t4
	jal exp
	
	mul $t0, $t0, $v0
	add $t7, $t7, $t0
	
	addi $t4, $t4, 1
	b atoi_loop

atoi_nan:
	# if not a number, just return the original array address
	move $v0, $t6
	b atoi_done

atoi_is_num:
	move $v0, $t7

atoi_done:
	move $ra, $t5
	jr $ra

exp:
	# $a0: number to take power of
	# $a1: power to raise it by (must be positive or zero)
	# 
	# $v0: a0 to the power of a1 
	# 
	# $t9: temp check if a1 is one
	
	move $s3, $a0
	move $t9, $a1
	addi $t9, $t9, -1
	beqz $t9, exp_one
	
	li $v0, 1
	li $t8, 1

exp_loop:
	beqz $a1, exp_done
	mul $t8, $t8, $s3
	
	addi $a1, $a1, -1
	b exp_loop

exp_one:
	move $v0, $s3
	jr $ra

exp_done:
	move $v0, $t8
	jr $ra