####################################################
#                                                  #
#                 everything sucks                 #
#                                                  #
####################################################

CLASSPATH		:= jacc/build
CLASSPATHS		:= -cp $(CLASSPATH)

SOURCES			:= src
BUILD			:= build
BUILDS			:= -d $(BUILD)

JAVAC			:= javac
JAVACFLAGS		:= $(BUILDS) $(CLASSPATHS)

TARGET			:= Simp.jar



.PHONY: all dev clean



all: submodule dev jar

dev: | $(BUILD)
	@echo "compiling..."
	
	@$(JAVAC) $(JAVACFLAGS) src/*.java -Xlint:unchecked
	@cp -r jacc/build/* build/

jar:
	@cd $(BUILD) && jar cvfe ../$(TARGET) Simp *
	@chmod +x Simp.jar

submodule:
	@echo "updating submodules..."
	@git submodule init
	@git submodule update --remote --merge || (make clean && make)
	
	@cd jacc && make -j6

reset-sub:
	git submodule deinit --all --force

$(BUILD):
	@mkdir $@

run:
	@java -jar $(TARGET)

clean: reset-sub
	@echo "clean ..."
	
	@rm -rf $(BUILD)
	@rm -f $(TARGET)