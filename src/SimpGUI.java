public class SimpGUI
{
	static javax.swing.JTextField jTextField1;
	static javax.swing.JTextField jTextField2;
	
	static javax.swing.JTextArea jTextArea1;
	
	public static javax.swing.JPanel createPanel()
	{
		javax.swing.JPanel pane;
		
		javax.swing.JButton jButton1;
		javax.swing.JButton jButton2;
		javax.swing.JButton jButton3;
		javax.swing.JLabel jLabel1;
		javax.swing.JLabel jLabel2;
		javax.swing.JScrollPane jScrollPane1;
		
		pane = new javax.swing.JPanel();
		
		jTextField1 = new javax.swing.JTextField();
		jButton1 = new javax.swing.JButton();
		jLabel1 = new javax.swing.JLabel();
		jTextField2 = new javax.swing.JTextField();
		jButton2 = new javax.swing.JButton();
		jLabel2 = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		jTextArea1 = new javax.swing.JTextArea();
		jButton3 = new javax.swing.JButton();

		jTextArea1.setEnabled(false);

		jTextField1.setText("");
		jTextField1.setEnabled(false);

		jButton1.setText("Browse...");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		jLabel1.setText("MIPS File");

		jTextField2.setText("");
		jTextField2.setEnabled(false);

		jButton2.setText("Browse...");
		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton2ActionPerformed(evt);
			}
		});

		jLabel2.setText(".smp File");

		jTextArea1.setColumns(20);
		jTextArea1.setRows(5);
		jScrollPane1.setViewportView(jTextArea1);

		jButton3.setText("Run");
		jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton3ActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(pane);
		pane.setLayout(layout);
		layout.setHorizontalGroup(
			layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
				.addContainerGap(49, Short.MAX_VALUE)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(layout.createSequentialGroup()
						.addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jButton2))
					.addGroup(layout.createSequentialGroup()
						.addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(jButton1)))
				.addGap(52, 52, 52))
			.addGroup(layout.createSequentialGroup()
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
					.addGroup(layout.createSequentialGroup()
						.addGap(160, 160, 160)
						.addComponent(jButton3))
					.addGroup(layout.createSequentialGroup()
						.addGap(167, 167, 167)
						.addComponent(jLabel2))
					.addGroup(layout.createSequentialGroup()
						.addGap(169, 169, 169)
						.addComponent(jLabel1)))
				.addGap(0, 162, Short.MAX_VALUE))
			.addGroup(layout.createSequentialGroup()
				.addContainerGap()
				.addComponent(jScrollPane1)
				.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
			.addGroup(layout.createSequentialGroup()
				.addGap(18, 18, 18)
				.addComponent(jLabel1)
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
					.addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addComponent(jButton1))
				.addGap(18, 18, 18)
				.addComponent(jLabel2)
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
				.addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
					.addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
					.addComponent(jButton2))
				.addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
				.addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
				.addGap(18, 18, 18)
				.addComponent(jButton3)
				.addContainerGap(21, Short.MAX_VALUE))
		);
		
		return pane;
	}
	
	private static void jButton3ActionPerformed(java.awt.event.ActionEvent evt)
	{
		String mipsPath = jTextField2.getText();
		String smpPath = jTextField1.getText();
		
		if (!mipsPath.isEmpty() && !smpPath.isEmpty())
		{
			jTextArea1.setText(Simp.test(jTextField2.getText(), jTextField1.getText()));
		}
	}
	
	private static void jButton2ActionPerformed(java.awt.event.ActionEvent evt)
	{
		jTextField2.setText(Simp.findFile());
	}
	
	private static void jButton1ActionPerformed(java.awt.event.ActionEvent evt)
	{
		jTextField1.setText(Simp.findFile());
	}
}