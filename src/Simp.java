// Copyright 2022 LittleCube

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JFileChooser;
import javax.swing.UIManager;

import java.util.Scanner;

public class Simp
{
	static JFrame frame;
	
	static String spim;
	static String exceptions;
	static String shell;
	static String shellArg;
	
	public static void main(String[] args)
	{
		String osName = System.getProperty("os.name");
		
		if (osName.toLowerCase().contains("win"))	// Windows
		{
			spim = ".\\spim\\spim.exe";
			exceptions = "spim\\exceptions.s";
			shell = "cmd";
			shellArg = "/c";
			
			try
			{
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			}
			
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
		
		else										// a good os
		{
			spim = "./spim/spim";
			exceptions = "spim/exceptions.s";
			shell = "sh";
			shellArg = "-c";
		}
		
		if (args.length > 1)
		{
			System.out.println(test(args[1], args[0]));
		}
		
		else
		{
			System.out.println("cli usage: java -jar Simp.jar [.s file] [.smp file]");
			
			JPanel pane = SimpGUI.createPanel();
			
			frame = new JFrame("Simp");
			
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			frame.add(pane);
			frame.pack();
			frame.setLocationRelativeTo(null);
			frame.setResizable(false);
			frame.setVisible(true);
		}
	}
	
	public static String test(String testPath, String mipsFile)
	{
		String output = "";
		
		Parser p = new Parser(testPath, true);
		
		p.addDelimiter(",");
		p.addDelimiter(";");
		p.addDelimiter("\n");
		
		LinkedList<LinkedList<String>> inputs = new LinkedList<LinkedList<String>>();
		inputs.add(new LinkedList<String>());
		
		LinkedList<String> outputs = new LinkedList<String>();
		
		int lineNo = 0;
		int inNo = 0;
		String delimiter;
		String nextNum = " ";
		
		while (!nextNum.isEmpty())
		{
			delimiter = p.nextDelimiter();
			
			nextNum = p.nextToken();
			
			if (delimiter.contains(";"))
			{
				outputs.add(nextNum);
				
				inputs.add(new LinkedList<String>());
				
				if (inputs.size() > 1)
				{
					inputs.next();
				}
			}
			
			else
			{
				inputs.current().add(nextNum);
			}
		}
		
		inputs.end();
		inputs.remove();
		
		try
		{
			String spimCmd = "";
			inputs.reset();
			
			boolean error = false;
			
			while (inputs.more() && !error)
			{
				spimCmd = spim + " -exception_file " + exceptions + " -file ";
				spimCmd += mipsFile + " ";
				
				inputs.current().reset();
				
				while (inputs.current().more())
				{
					spimCmd += inputs.current().current() + " ";
					
					inputs.current().next();
				}
				
				Process pr = Runtime.getRuntime().exec(new String[] { shell, shellArg, spimCmd });
				
				Scanner s = new Scanner(pr.getInputStream());
				String result = "";
				
				while (s.hasNext())
				{
					// get rid of stupid exception loaded line
					s.nextLine();
					
					if (s.hasNext())
					{
						result = s.nextLine();
						
						String expected = outputs.current();
						outputs.next();
						
						output += "expected: \"" + expected + "\" actual: \"" + result + "\" ";
						
						if (result.equals(expected))
						{
							output += "PASSED\n\n";
						}
						
						else
						{
							output += "FAILED\n\n";
						}
					}
					
					else
					{
						output += "Error: no output printed.\n       Your MIPS code should print your result to the console.\n";
						error = true;
						break;
					}
				}
				
				s.close();
				pr.destroy();
				
				inputs.next();
			}
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		output = output.substring(0, output.length() - 1);
		
		return output;
	}
	
	public static String findFile()
	{
		JFileChooser jfc = new JFileChooser();
		jfc.setDragEnabled(true);
		String path = "";
		
		try
		{
			if (jfc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION)
			{
				path = jfc.getSelectedFile().getAbsolutePath();
			}
		}
		
		catch (Exception e)
		{
			e.printStackTrace();
			
			SimpGUI.jTextArea1.setText(e.toString());
		}
		
		return path;
	}
}